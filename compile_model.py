from d3m import index
from d3m.metadata.base import ArgumentType
from d3m.metadata.pipeline import Pipeline, PrimitiveStep

import d3m.primitives.data_preprocessing.image_reader
import d3m.primitives.data_transformation.denormalize
import d3m.primitives.data_transformation.dataset_to_dataframe
import d3m.primitives.data_transformation.construct_predictions
import d3m.primitives.data_transformation.extract_columns_by_semantic_types
import d3m.primitives.data_transformation.replace_semantic_types
from constants import step_function, FORWARD_LAYERS, ACTIVATIONS

import d3m.primitives.loss_function.categorical_crossentropy
import d3m.primitives.loss_function.categorical_accuracy

import d3m.primitives.learner.model
import d3m.primitives.data_wrangling.batching
import pickle

NET_SETUP_IDX = IP_STEP = OP_STEP = READER_STEP = -1
BATCH_SIZE = 40


def data_setup(pipeline_description):
    global IP_STEP, OP_STEP, READER_STEP

    # denormalize
    denorm_step_idx = 0
    step = PrimitiveStep(
        primitive_description=d3m.primitives.data_transformation.denormalize.Common.metadata.query())
    step.add_argument(
        name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='inputs.0')
    step.add_output('produce')
    pipeline_description.add_step(step)

    # dataset_to_dataframe
    dataset_to_dataframe_step_idx = len(pipeline_description.steps)
    step = PrimitiveStep(
        primitive_description=d3m.primitives.data_transformation.dataset_to_dataframe.Common.metadata.query())
    step.add_argument(
        name='inputs', argument_type=ArgumentType.CONTAINER,
        data_reference='steps.{}.produce'.format(denorm_step_idx))
    step.add_output('produce')
    pipeline_description.add_step(step)

    # extract targets
    extract_step_idx = len(pipeline_description.steps)
    extract_targets = PrimitiveStep(
        d3m.primitives.data_transformation.extract_columns_by_semantic_types.Common.metadata.query())
    extract_targets.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER,
                                 data_reference='steps.{}.produce'.format(dataset_to_dataframe_step_idx))
    extract_targets.add_hyperparameter(name='semantic_types', argument_type=ArgumentType.VALUE,
                                       data=['https://metadata.datadrivendiscovery.org/types/TrueTarget'])
    extract_targets.add_output('produce')
    pipeline_description.add_step(extract_targets)

    # replace semantic types
    # Need to be used for CIFAR-10
    replace_step_idx = len(pipeline_description.steps)
    replace_semantic = PrimitiveStep(
        d3m.primitives.data_transformation.replace_semantic_types.Common.metadata.query())
    replace_semantic.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER,
                                  data_reference=f'steps.{extract_step_idx}.produce')
    replace_semantic.add_hyperparameter(name='to_semantic_types', argument_type=ArgumentType.VALUE,
                                        data=['https://metadata.datadrivendiscovery.org/types/SuggestedTarget',
                                              'https://metadata.datadrivendiscovery.org/types/TrueTarget'])
    replace_semantic.add_hyperparameter(name='from_semantic_types', argument_type=ArgumentType.VALUE,
                                        data=['https://metadata.datadrivendiscovery.org/types/TrueTarget'])
    replace_semantic.add_output('produce')
    pipeline_description.add_step(replace_semantic)

    # image reader
    reader_step_idx = len(pipeline_description.steps)
    reader = PrimitiveStep(
        primitive_description=d3m.primitives.data_preprocessing.image_reader.Common.metadata.query())
    reader.add_hyperparameter(name='return_result', argument_type=ArgumentType.VALUE, data='new')
    pipeline_description.add_step(reader)

    IP_STEP, OP_STEP, READER_STEP = dataset_to_dataframe_step_idx, replace_step_idx, reader_step_idx


def neural_net_setup(pipeline_description):
    global NET_SETUP_IDX

    NET_SETUP_IDX = len(pipeline_description.steps)
    step = PrimitiveStep(
        primitive_description=d3m.primitives.loss_function.categorical_crossentropy.KerasWrap.metadata.query())
    pipeline_description.add_step(step)


def assemble(pipeline_description, batch_size=BATCH_SIZE):
    learner_idx = len(pipeline_description.steps)
    step = PrimitiveStep(primitive_description=d3m.primitives.learner.model.KerasWrap.metadata.query())
    step.add_hyperparameter(name='loss', argument_type=ArgumentType.PRIMITIVE, data=NET_SETUP_IDX)
    step.add_hyperparameter(name='model_type', argument_type=ArgumentType.VALUE, data='classification')
    step.add_hyperparameter(name='network_last_layer', argument_type=ArgumentType.PRIMITIVE,
                            data=learner_idx - 1)
    step.add_hyperparameter(name='return_result', argument_type=ArgumentType.VALUE, data='replace')
    lr = 0.0001
    adam_hypers = d3m.primitives.learner.model.KerasWrap.metadata.get_hyperparams().defaults(path='optimizer.Adam')
    adam_hypers = adam_hypers.replace({'lr': lr})
    step.add_hyperparameter(name='optimizer', argument_type=ArgumentType.VALUE, data=adam_hypers)
    pipeline_description.add_step(step)

    bz_loader = PrimitiveStep(primitive_description=d3m.primitives.data_wrangling.batching.TAMU.metadata.query())
    bz_loader.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER,
                           data_reference=f'steps.{IP_STEP}.produce')
    bz_loader.add_argument(name='outputs', argument_type=ArgumentType.CONTAINER,
                           data_reference='steps.{}.produce'.format(OP_STEP))
    bz_loader.add_hyperparameter(name='primitive_reader', argument_type=ArgumentType.PRIMITIVE, data=READER_STEP)
    bz_loader.add_hyperparameter(name='primitive_learner', argument_type=ArgumentType.PRIMITIVE, data=learner_idx)
    bz_loader.add_hyperparameter(name='batch_size', argument_type=ArgumentType.VALUE, data=batch_size)
    bz_loader.add_hyperparameter(name='sampling_method', argument_type=ArgumentType.VALUE, data='random')
    bz_loader.add_output('produce')

    pipeline_description.add_step(bz_loader)

def predictions(pipeline_description):
    pred = PrimitiveStep(
        primitive_description=d3m.primitives.data_transformation.construct_predictions.Common.metadata.query())
    pred.add_argument(
        name='inputs', argument_type=ArgumentType.CONTAINER,
        data_reference=f"steps.{len(pipeline_description.steps) - 1}.produce"
    )
    pred.add_argument(name='reference', argument_type=ArgumentType.CONTAINER,
                        data_reference='steps.{}.produce'.format(IP_STEP))
    pred.add_output('produce')
    pipeline_description.add_step(pred)


def get_previous_layer_ids(graph, ordered_layers_ids):
    previous_layer_ids = {}
    reverse_adj_list = graph.reverse_adj_list
    layer_id_to_input_node_ids = graph.layer_id_to_input_node_ids
    layer_list = graph.layer_list
    for current_layer_id in ordered_layers_ids:
        input_node_ids = layer_id_to_input_node_ids[current_layer_id]
        previous_layer_ids[current_layer_id] = set()
        for node_id in input_node_ids:
            for previous_node_id, previous_layer_id in reverse_adj_list[node_id]:
                prev_layer = layer_list[previous_layer_id]
                if prev_layer.str == 'ReLU':
                    previous_layer_ids[current_layer_id].update(previous_layer_ids[previous_layer_id])
                else:
                    previous_layer_ids[current_layer_id].add(previous_layer_id)
    return previous_layer_ids


def get_topological_layer_ids(graph):
    ordered_layers = []
    reverse_adj_list = graph.reverse_adj_list
    for node_id in graph.topological_order:
        layer_id = set()
        if node_id == 0:
            continue
        for previous_node_id, previous_layer_id in reverse_adj_list[node_id]:
            layer_id.add(previous_layer_id)
        ordered_layers.append(tuple(layer_id)[0])
    return ordered_layers


def get_pipeline(layers, previous_layer_ids, ordered_layers_ids):
    """
    A function that returns the pipeline in the YAML format.
    :param layers: A list of AutoKeras layers in a neural network.
    :return: An object of pipeline.
    """
    # Creating pipeline
    pipeline_description = Pipeline()
    pipeline_description.add_input(name='inputs')

    data_setup(pipeline_description)
    neural_net_setup(pipeline_description)

    avoid_layers = set([layer_idx for layer_idx in range(len(layers)) if layers[layer_idx].str in ACTIVATIONS])
    offset = len(pipeline_description.steps)

    compiled_layers = []
    flatten_done = False
    compiled_layer_idx = 0
    layer_id_to_compiled_id = {0: None}
    total_layer_num = len(layers)
    for i in ordered_layers_ids:
        if i not in avoid_layers:
            layer = layers[i]
            layer.previous_layer_ids = [None] if len(previous_layer_ids[i]) == 0 else tuple(previous_layer_ids[i])
            if layer.str in FORWARD_LAYERS and i != len(layers):
                layer.activation = 'relu'
                if layer.str == 'Dense' and not flatten_done:
                    compiled_layers.append(step_function['Flatten'](i, layer))
                    flatten_done = True
                    flat_layer_id = total_layer_num
                    total_layer_num += 1
                    layer_id_to_compiled_id[flat_layer_id] = compiled_layer_idx
                    layer.previous_layer_ids = (flat_layer_id, )
                    compiled_layer_idx += 1
            if layer.str in ('Add', 'Concatenate') and len(layer.previous_layer_ids) == 1:
                compiled_layers.append(step_function['Null'](i))
                null_layer_id = total_layer_num
                total_layer_num += 1
                layer_id_to_compiled_id[null_layer_id] = compiled_layer_idx
                layer.previous_layer_ids = (layer.previous_layer_ids[0], null_layer_id)
            compiled_layers.append(step_function[layer.str](i, layer))
            layer_id_to_compiled_id[i] = compiled_layer_idx
            compiled_layer_idx += 1

    for layer in compiled_layers:
        hyperparam_to_remove = None
        for hyperparameter in layer.hyperparams.keys():
            if layer.hyperparams[hyperparameter]['type'] == ArgumentType.PRIMITIVE:
                data = layer.hyperparams[hyperparameter]['data']
                if isinstance(data, tuple):
                    layer.hyperparams[hyperparameter]['data'] = tuple(layer_id_to_compiled_id[i] + offset for i in data)
                else:
                    compiled_layer_id = layer_id_to_compiled_id[data]
                    layer.hyperparams[hyperparameter]['data'] = compiled_layer_id + offset
        if hyperparam_to_remove is not None:
            del layer.hyperparams[hyperparam_to_remove]
        # print(layer.to_json_structure())
        pipeline_description.add_step(layer)
        
    assemble(pipeline_description)
    predictions(pipeline_description)
    pipeline_description.add_output(
        name='output predictions', data_reference=f"steps.{len(pipeline_description.steps) - 1}.produce")

    return pipeline_description


if __name__ == "__main__":
    AK_LAYERS_FILE = './layers1.pkl'
    ak_layers = pickle.load(open(AK_LAYERS_FILE, 'rb'))
    pipeline = get_pipeline(ak_layers)
    pipeline_yaml = pipeline.to_yaml()
    print(pipeline_yaml)
