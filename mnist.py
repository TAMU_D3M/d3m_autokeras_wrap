"""
Search for a good model for the
[MNIST](https://keras.io/datasets/#mnist-database-of-handwritten-digits) dataset.
"""

from tensorflow.keras.datasets import mnist
from tensorflow.keras.utils import plot_model
# from autokeras.tuners import task_specific

import autokeras as ak
# task_specific.IMAGE_CLASSIFIER = [{
#     'image_block_1/block_type': 'resnet',
#     'image_block_1/normalize': True,
#     'image_block_1/augment': True,
#     'image_block_1/res_net_block_1/version': 'v2',
#     'image_block_1/res_net_block_1/pooling': 'avg',
#     'image_block_1/res_net_block_1/conv3_depth': 4,
#     'image_block_1/res_net_block_1/conv4_depth': 6,
#     'dense_block_1/num_layers': 2,
#     'dense_block_1/use_batchnorm': False,
#     'dense_block_1/dropout_rate': 0,
#     'dense_block_1/units_0': 32,
#     'dense_block_1/units_1': 32,
#     'classification_head_1/dropout_rate': 0,
#     'optimizer': 'adam'
# }]

# Prepare the dataset.
(x_train, y_train), (x_test, y_test) = mnist.load_data()
print(x_train.shape)  # (60000, 28, 28)
print(y_train.shape)  # (60000,)
print(y_train[:3])  # array([7, 2, 1], dtype=uint8)

# Initialize the ImageClassifier.
clf = ak.ImageClassifier(max_trials=1, seed=1)
# Search for the best model.
clf.fit(x_train, y_train, epochs=1)
model = clf.export_model()
path = 'ak1.0.2_resnet'
model.save(path)
file = '/opt/project/ak1.0.2_resnet.png'
plot_model(model, to_file=file)

# Evaluate on the testing data.
print('Accuracy: {accuracy}'.format(
    accuracy=clf.evaluate(x_test, y_test)))
