FROM registry.gitlab.com/tamu_d3m/winter_2018_tamuta2/primitives:latest
LABEL maintainer="Yi-Wei Chen <yiwei_chen@tamu.edu>"

RUN unlink /usr/local/cuda

# base https://gitlab.com/nvidia/container-images/cuda/-/blob/master/dist/ubuntu18.04/10.1/base/Dockerfile
# runtime https://gitlab.com/nvidia/container-images/cuda/-/blob/master/dist/ubuntu18.04/10.1/runtime/Dockerfile
# devel https://gitlab.com/nvidia/container-images/cuda/-/blob/master/dist/ubuntu18.04/10.1/devel/Dockerfile
# D3M images https://gitlab.com/datadrivendiscovery/images/-/blob/master/base/ubuntu-bionic-python36.dockerfile
# https://www.tensorflow.org/install/gpu#install_cuda_with_apt

# remove existing soft link
RUN rm -rf /var/lib/apt/lists/*

ENV CUDA_VERSION 10.1.243
ENV CUDA_PKG_VERSION 10-1=$CUDA_VERSION-1
ENV NCCL_VERSION 2.4.8
ENV CUDNN_VERSION 7.6.5.32
ENV NVINFER_VERSION 6.0.1

# For libraries in the cuda-compat-* package: https://docs.nvidia.com/cuda/eula/index.html#attachment-a
RUN apt-get update && apt-get install -y --no-install-recommends \
    cuda-cudart-$CUDA_PKG_VERSION \
    cuda-compat-10-1 \
    cuda-command-line-tools-$CUDA_PKG_VERSION \
    libcublas10=10.2.1.243-1 \
    cuda-libraries-$CUDA_PKG_VERSION \
    cuda-nvtx-$CUDA_PKG_VERSION \
    libcudnn7=$CUDNN_VERSION-1+cuda10.1 \
    cuda-libraries-dev-$CUDA_PKG_VERSION \
    cuda-nvml-dev-$CUDA_PKG_VERSION \
    cuda-minimal-build-$CUDA_PKG_VERSION \
    libcublas-dev=10.2.1.243-1 \
    libnccl2=$NCCL_VERSION-1+cuda10.1 \
    libnccl-dev=$NCCL_VERSION-1+cuda10.1 \
    libnvinfer6=$NVINFER_VERSION-1+cuda10.1 \
    cuda-cupti-$CUDA_PKG_VERSION \
    && \
    ln -s cuda-10.1 /usr/local/cuda && \
    rm -rf /var/lib/apt/lists/*

ENV LD_LIBRARY_PATH usr/local/cuda/extras/CUPTI/lib64:/usr/local/cuda/lib64:/usr/local/nvidia/lib:/usr/local/nvidia/lib64

# nvidia-container-runtime
ENV NVIDIA_REQUIRE_CUDA "cuda>=10.1 brand=tesla,driver>=384,driver<385 brand=tesla,driver>=396,driver<397 brand=tesla,driver>=410,driver<411"