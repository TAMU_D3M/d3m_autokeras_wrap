FROM registry.gitlab.com/tamu_d3m/winter_2018_tamuta2/primitives:latest
LABEL maintainer="Yi-Wei Chen <yiwei_chen@tamu.edu>"

# install virtualenv
RUN pip3 install virtualenv

COPY requirements.txt /tmp

ENV AK_VIRTUAL_ENV /opt/ak
RUN python3 -m virtualenv --python=/usr/bin/python3 --system-site-packages $AK_VIRTUAL_ENV

ENV PATH_ORI $PATH
ENV PATH "$AK_VIRTUAL_ENV/bin:$PATH"

# Install dependencies
RUN pip3 install -r /tmp/requirements.txt

ENV PATH $PATH_ORI