import argparse
import pickle

import autokeras as ak
from keras2d3m import get_pipeline

def argsparser():
    parser = argparse.ArgumentParser("AutoKeras")
    parser.add_argument('--x', help='The path to pickle file of x', default='./pickle/attributes.pickle')
    parser.add_argument('--y', help='The path to pickle file of y', default='./pickle/targets.pickle')
    parser.add_argument('--output', help='Expected path of output model', default='./model.json')
    parser.add_argument('--directory', help='The path to a directory for storing the search outputs',
                        default='.')
    parser.add_argument('--max_trials', help='The maximum number of trials', type=int, default=1)
    parser.add_argument('--seed', help='The random seed', type=int, default=1)
    parser.add_argument('--epochs', help='The number epochs to be trained', type=int, default=1)
    parser.add_argument('--batch_size', help='The batch size during training', type=int, default=32)
    parser.add_argument('--validation_split', help='The ratio used for validation', type=float, default=0.2)
    return parser

def run():
    parser = argsparser()
    args = parser.parse_args()

    # Load pickle
    with open(args.x, 'rb') as f:
        x = pickle.load(f)
    with open(args.y, 'rb') as f:
        y = pickle.load(f)

    # Train AK
    clf = ak.ImageClassifier(max_trials=args.max_trials, seed=args.seed, directory=args.directory)
    clf.fit(x=x,
            y=y,
            epochs=args.epochs,
            batch_size=args.batch_size,
            validation_split=args.validation_split)
    keras_model = clf.export_model()
    d3m_pipeline = get_pipeline(keras_model, args.batch_size)

    with open(args.output, 'w') as f:
        d3m_pipeline.to_json(f, nest_subpipelines=True, indent=2, sort_keys=True, ensure_ascii=False)
 
if __name__ == "__main__":
    run()