# d3m_autokeras_wrap

This wrapper converts models from AutoKeras to a runnable D3M pipeline.

## Dataset
* [124_120_mnist]()  
* [124_174_cifar10_MIN_METADATA](https://gitlab.datadrivendiscovery.org/d3m/datasets/tree/master/seed_datasets_current/124_174_cifar10_MIN_METADATA)  
Or ask D3M project participants to get the datasets
 
## Docker image
`docker pull registry.gitlab.com/tamu_d3m/d3m_autokeras_wrap/primitives:ak`  
`docker run --gpus '"device=1,2"' -it registry.gitlab.com/tamu_d3m/d3m_autokeras_wrap/primitives:ak`

## PyCharm & Docker
You can make use of Docker engine in remote server (e.g., datalab3) by the following steps.  
Preference -> Project Interpreter -> Add -> Docker -> New -> Engine API URL  
(Please ask the server owner for the port number.)  
<img src="img/pycharm_docker.png"  width="" height="450">


<!---
# Usage:

## Generating an AutoKeras model

`generate_model.py` shows you an example of how to generate an AutoKeras model for the MNIST dataset. You have to substitute this
for your dataset. The generate model is stored in the directory `autokeras_model` under the filename `model.pkl`.

## Loading the AutoKeras model

`load.py` maps the AutoKeras model generated into a list consisting the layers of the network. These layers are pickled in 
`layers.pkl`.

## Generating the YAML

`main.py` uses the network's layers in `layers.pkl` to generate a YAML pipeline.
--->