from autokeras.utils import pickle_from_file, pickle_to_file

# Customer temp dir by your own
TEMP_DIR = './autokeras_model'
model_file_name = f'{TEMP_DIR}/model.pkl'

if __name__ == '__main__':
    # (x_train, y_train), (x_test, y_test) = mnist.load_data()
    # x_train = x_train.reshape(x_train.shape + (1,))
    # x_test = x_test.reshape(x_test.shape + (1,))
    model = pickle_from_file(model_file_name)

    for layer in model.graph.layer_list:
        layer.str = str(layer)
        if layer.str.startswith('Conv'):
            layer.str = layer.str.split('(')[0]

    pickle_to_file(model.graph.layer_list, 'layers.pkl')
