from d3m.metadata.pipeline import Pipeline, PrimitiveStep
import d3m.primitives.data_transformation.dataset_to_dataframe
import d3m.primitives.data_transformation.column_parser
import d3m.primitives.data_transformation.construct_predictions
import d3m.primitives.data_transformation.extract_columns_by_semantic_types
import d3m.primitives.data_cleaning.imputer
import d3m.primitives.data_transformation.denormalize
import d3m.primitives.data_preprocessing.text_reader

import d3m.primitives.data_preprocessing.image_reader
from typing import List, Dict, Any
import d3m.primitives.regression.elastic_net

import d3m.primitives.learner.model

import d3m.primitives.layer.flatten
import d3m.primitives.layer.convolution_2d
import d3m.primitives.layer.max_pooling_2d
import d3m.primitives.layer.dropout
import d3m.primitives.layer.dense
import d3m.primitives.layer.batch_normalization
import d3m.primitives.layer.add

import d3m.primitives.loss_function.categorical_crossentropy
import d3m.primitives.loss_function.categorical_accuracy

import inspect
from d3m.metadata.base import ArgumentType, Context

pipeline_description = Pipeline(context=Context.TESTING)
pipeline_description.add_input(name='inputs')

####################################
#           Data Setup             #
####################################

# Step 0: Denormalize
step_0 = PrimitiveStep(
    primitive_description=d3m.primitives.data_transformation.denormalize.Common.metadata.query())
step_0.add_argument(
    name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='inputs.0')
step_0.add_output('produce')
pipeline_description.add_step(step_0)

# Step 1: DatasetToDataFrame
step_1 = PrimitiveStep(
    primitive_description=d3m.primitives.data_transformation.dataset_to_dataframe.Common.metadata.query())
step_1.add_argument(
    name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.0.produce')
step_1.add_output('produce')
pipeline_description.add_step(step_1)


# Step 2: ImageReader
step_2 = PrimitiveStep(
    primitive_description=d3m.primitives.data_preprocessing.image_reader.DataFrameCommon.metadata.query())
step_2.add_argument(
    name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.1.produce')
step_2.add_output('produce')
pipeline_description.add_step(step_2)


####################################
#        Neural Net Setup          #
####################################
STARTING_NET_SETUP_IDX = 3
# Loss function
step_3 = PrimitiveStep(
    primitive_description=d3m.primitives.loss_function.categorical_crossentropy.KerasWrap.metadata.query())
pipeline_description.add_step(step_3)

# Metrics
step_4 = PrimitiveStep(
    primitive_description=d3m.primitives.loss_function.categorical_accuracy.KerasWrap.metadata.query())
pipeline_description.add_step(step_4)


def create_linear_arch(pipe: Pipeline, layers: List[d3m.primitive_interfaces.base.PrimitiveBaseMeta]):
    for idx, module in enumerate(layers):
        layer = module['module']
        hyperparams = module['hyperparams']
        if inspect.isclass(layer) and issubclass(layer, Block):
            layer.process_block(pipe, hyperparams=hyperparams)
        else:
            _layer_step = PrimitiveStep(
                primitive_description=layer.KerasWrap.metadata.query())
            if idx != 0:
                _layer_step.add_hyperparameter(name='previous_layer', argument_type=ArgumentType.PRIMITIVE,
                                               data=len(pipe.steps)-1)
                for key, val in hyperparams.items():
                    _layer_step.add_hyperparameter(
                        name=key, argument_type=ArgumentType.VALUE, data=val)
            else:
                # For the Very first layer primitivek we want to keep None
                pass
            pipe.add_step(_layer_step)

    return pipe


class Block:

    def __init__(self):
        pass

    def process_block(self):
        raise NotImplementedError()


class ResnetSkipBlock(Block):

    def __init__(self):
        pass

    @classmethod
    def process_block(self, pipe: Pipeline, hyperparams: Dict[str, Any] = {}):
        current_before_pipe_idx = len(pipe.steps) - 1

        _conv1 = PrimitiveStep(
            primitive_description=d3m.primitives.layer.convolution_2d.KerasWrap.metadata.query())
        _conv1.add_hyperparameter(name='previous_layer', argument_type=ArgumentType.PRIMITIVE,
                                  data=current_before_pipe_idx)
        _conv1.add_hyperparameter(name='padding', argument_type=ArgumentType.VALUE,
                                  data='same')
        _conv1.add_hyperparameter(name='filters', argument_type=ArgumentType.VALUE,
                                  data=50)
        _conv1.add_hyperparameter(name='strides', argument_type=ArgumentType.VALUE,
                                  data=2)
        pipe.add_step(_conv1)

        _bn1 = PrimitiveStep(
            primitive_description=d3m.primitives.layer.batch_normalization.KerasWrap.metadata.query())
        _bn1.add_hyperparameter(name='previous_layer', argument_type=ArgumentType.PRIMITIVE,
                                data=len(pipe.steps)-1)
        pipe.add_step(_bn1)

        _conv1 = PrimitiveStep(
            primitive_description=d3m.primitives.layer.convolution_2d.KerasWrap.metadata.query())
        _conv1.add_hyperparameter(name='previous_layer', argument_type=ArgumentType.PRIMITIVE,
                                  data=len(pipe.steps)-1)
        _conv1.add_hyperparameter(name='padding', argument_type=ArgumentType.VALUE,
                                  data='same')
        _conv1.add_hyperparameter(name='filters', argument_type=ArgumentType.VALUE,
                                  data=50)
        _conv1.add_hyperparameter(name='strides', argument_type=ArgumentType.VALUE,
                                  data=2)
        pipe.add_step(_conv1)

        _bn1 = PrimitiveStep(
            primitive_description=d3m.primitives.layer.batch_normalization.KerasWrap.metadata.query())
        _bn1.add_hyperparameter(name='previous_layer', argument_type=ArgumentType.PRIMITIVE,
                                data=len(pipe.steps)-1)
        pipe.add_step(_bn1)

        _add = PrimitiveStep(
            primitive_description=d3m.primitives.layer.add.KerasWrap.metadata.query())
        _add.add_hyperparameter(name='previous_layers', argument_type=ArgumentType.PRIMITIVE,
                                data=[len(pipe.steps)-1,
                                      current_before_pipe_idx])
        pipe.add_step(_add)

        return pipe


layers = [
    {"module": d3m.primitives.layer.convolution_2d, "hyperparams": {}},
    {"module": ResnetSkipBlock, "hyperparams": {}},
    {"module": ResnetSkipBlock, "hyperparams": {}},
    {"module": d3m.primitives.layer.flatten, "hyperparams": {}},
    {"module": d3m.primitives.layer.dense, "hyperparams": {'units': 100}},
    {"module": d3m.primitives.layer.dense, "hyperparams": {'units': 100}},
    {"module": d3m.primitives.layer.dense, "hyperparams": {'units': 10}}
]

pipeline_description = create_linear_arch(
    pipe=pipeline_description, layers=layers)


# Assembly -- (idx pip)
step_8 = PrimitiveStep(
    primitive_description=d3m.primitives.learner.model.KerasWrap.metadata.query())
step_8.add_argument(
    name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.2.produce')
step_8.add_argument(
    name='outputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.2.produce')
step_8.add_hyperparameter(name='loss', argument_type=ArgumentType.PRIMITIVE,
                          data=STARTING_NET_SETUP_IDX)
step_8.add_hyperparameter(name='metric', argument_type=ArgumentType.PRIMITIVE,
                          data=STARTING_NET_SETUP_IDX+1)
step_8.add_hyperparameter(name='model_type', argument_type=ArgumentType.VALUE,
                          data='classification')
step_8.add_hyperparameter(name='previous_layer', argument_type=ArgumentType.PRIMITIVE,
                          data=len(pipeline_description.steps)-1)
step_8.add_hyperparameter(name='return_result', argument_type=ArgumentType.VALUE,
                          data='replace')
# Assembly main hyperparameter

lr = 0.0001
# epochs = 12 # Can't be used because runtime doesn't support setting fit iterations or continue_fit yet

adam_hypers = d3m.primitives.learner.model.KerasWrap.metadata.get_hyperparams(
).defaults(path='optimizer.Adam')
adam_hypers = adam_hypers.replace({'lr': lr})

step_8.add_hyperparameter(name='optimizer', argument_type=ArgumentType.VALUE,
                          data=adam_hypers)
step_8.add_output('produce')
pipeline_description.add_step(step_8)


####################################
#        Construct Outputs         #
####################################

# Step: n-1 Construct Predictions
step_9 = PrimitiveStep(
    primitive_description=d3m.primitives.data_transformation.construct_predictions.DataFrameCommon.metadata.query())
step_9.add_argument(
    name='inputs', argument_type=ArgumentType.CONTAINER, data_reference=f"steps.{len(pipeline_description.steps)-1}.produce"
)
step_9.add_argument(
    name='reference', argument_type=ArgumentType.CONTAINER, data_reference='steps.1.produce'
)
step_9.add_output('produce')
pipeline_description.add_step(step_9)

# Final Output
pipeline_description.add_output(
    name='output predictions', data_reference=f"steps.{len(pipeline_description.steps)-1}.produce")

pipeline_json = pipeline_description.to_yaml()
print(pipeline_json)


with open("tmp/tmp_pipeline.yml", "w") as fw:
    fw.write(pipeline_description.to_yaml())