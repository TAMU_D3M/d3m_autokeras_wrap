import os
import numpy as np
from tensorflow.keras.preprocessing.image import load_img, img_to_array
import pandas as pd
from autokeras import ImageClassifier
import sys
from compile_model import get_pipeline, get_previous_layer_ids, get_topological_layer_ids


def get_img_as_array(image_file):
    img = load_img(image_file)
    img_array = img_to_array(img, data_format='channels_last')
    return img_array


def get_labels(files):
    a = pd.read_csv(learning_data_csv, index_col='image')
    return np.array([a.at[f, 'label'] for f in files])


def get_data():
    x_train, x_test, y_train, y_test = [], [], [], []
    learning_data = pd.read_csv(learning_data_csv, index_col='d3mIndex')
    data_split = pd.read_csv(data_splits_csv, index_col='d3mIndex')
    df = learning_data.merge(data_split, left_on='d3mIndex', right_on='d3mIndex')
    for index, row in df.iterrows():
        if row['type'] =='TRAIN':
            x_train.append(get_img_as_array(os.path.join(media, row['image'])))
            y_train.append(int(row['label']))
        else:
            x_test.append(get_img_as_array(os.path.join(media, row['image'])))
            y_test.append(int(row['label']))
    return [np.array(arr) for arr in [x_train, x_test, y_train, y_test]]


if __name__ == '__main__':
    is_resume = False
    if len(sys.argv) != 2:
        print('Usage: python [filename] [name the dataset]')
    else:
        # all datasets should be located in the `datasets` directory
        dataset = sys.argv[1]  # relative path to the dataset directory
        dataset_dir = os.path.join('datasets', dataset, dataset+'_dataset')
        problem_dir = os.path.join('datasets', dataset, dataset+'_problem')
        dirname = os.path.dirname(__file__)
        dataset_dir = os.path.join(dirname, dataset_dir)
        problem_dir = os.path.join(dirname, problem_dir)

        media = os.path.join(dataset_dir, 'media')
        learning_data_csv = os.path.join(dataset_dir, 'tables/learningData.csv')
        data_splits_csv = os.path.join(problem_dir, 'dataSplits.csv')
        # TEMP_DIR = '/opt/project/search_id_small_mnist'
        TEMP_DIR = './autokeras_model_for_' + dataset + '_layer45'
        # TEMP_DIR = './autokeras_model_for_image_dataset_2_without_sampler'

        if is_resume:
            clf = ImageClassifier(verbose=True, augment=False, path=TEMP_DIR, resume=True)
        else:
            x_train, x_test, y_train, y_test = get_data()
            clf = ImageClassifier(verbose=True, augment=False, path=TEMP_DIR)
            clf.fit(x_train, y_train, time_limit=0.15 * 60 * 60)
        model = clf.export_autokeras_model(' ')

        for layer in model.graph.layer_list:
            layer.str = str(layer)
            if layer.str.startswith('Conv'):
                layer.str = layer.str.split('(')[0]

        # feed the model's layers to pipeline generator
        ordered_layers_ids = get_topological_layer_ids(model.graph)
        previous_layer_ids = get_previous_layer_ids(model.graph, ordered_layers_ids)
        pipeline = get_pipeline(model.graph.layer_list, previous_layer_ids, ordered_layers_ids)
        pipeline_yaml = pipeline.to_yaml()
        with open('ak_pipeline_mnist_1k_w_sampler_45.yml', 'w') as f:
            f.write(pipeline_yaml)
        print(pipeline_yaml)
